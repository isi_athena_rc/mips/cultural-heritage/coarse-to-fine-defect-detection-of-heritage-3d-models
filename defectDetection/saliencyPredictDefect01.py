from common.definitions import *
from config.configPredictDefect01 import *
from common.nets import CNNmodelKeras
from common import convert
saliency_model = CNNDeepmodelKeras(img_size, num_channels, num_classes, type)

print(keyTrain)
keyTrain=keyTrain+'_best_model'
saliency_model.load_weights("./models/" + keyTrain + ".h5")



print('Predict')
# ======Model information==============================================================================
modelName = 'pillar4RemeshedDefectsV2'

mModelSrc = rootdir + modelsDir + modelName + '.obj'
print(modelName)




if mode == "MESH":
    mModel = loadObj(mModelSrc)
    updateGeometryAttibutes(mModel, useGuided=useGuided, numOfFacesForGuided=patchSizeGuided, computeDeltas=False,
                            computeAdjacency=False, computeVertexNormals=False)
    iLen = len(mModel.faces)


if mode == "PC":
    mModel = loadObjPC(mModelSrc, nn=pointcloudnn, simplify=presimplification)
    V, inds = computePointCloudNormals(mModel, pointcloudnn)
    iLen = len(mModel.vertices)

gtdata=np.genfromtxt(rootdir + modelsDir + modelName+groundTruthKeyword+'.csv', delimiter=',')


saliencyValues=[]
print('Saliency ground truth data')
if type == 'continuous':
    # saliencyValues=gtdata.tolist()
    for v in gtdata.tolist():
        saliencyValues.append(v)
if type == 'discrete':
    for s in gtdata.tolist():
        v = int((num_classes - 1) * s)
        saliencyValues.append(v)


# for i, v in enumerate(mModel.vertices):
#     # h=-((resultPerVertex[i] * 240*0.25))
#     h = 0
#     # s=1.0
#     s = 0
#     # v=1.0
#     v = (resultPerVertex[i])
#     r, b, g = hsv2rgb(h, s, v)
#     mModel.vertices[i] = mModel.vertices[i]._replace(color=np.asarray([r / 255, g / 255, b / 255]))
#
# ffname = rootdir + modelsDir + modelName + "_" + reshapeFunction + "_pred" + "_cnn_" + "p_" + str(numOfElements)
# exportObj(mModel, ffname + ".obj", color=True)
# convert.execute(ffname + ".obj", ffname + ".ply")
# # os.remove(ffname + ".obj")




predict_data = np.empty([iLen, patchSide, patchSide, 3])
prediction = np.empty([iLen, 1])
print('Start')

# subset = random.sample(list(range(iLen)), int(0.1 * iLen))
# subset.sort()
# for ipointer in subset:
for ipointer in range(0, iLen, batchsize):
    pall = []
    istart = ipointer
    iend = min(ipointer + batchsize, iLen - 1)
    for i in range(istart, iend):
        if i % 2000 == 0:
            print('Extract patch information : ' + str(
                np.round((100 * i / iLen), decimals=2)) + ' ' + '%')

        if mode == "MESH":
            patchFacesOriginal = [mModel.faces[i] for i in neighboursByFace(mModel, i, numOfElements)[0]]
            # patchFacesOriginal = [mModel.faces[i] for i in mModel.faces[i].neighbouringFaceIndices64[:numOfElements]]
            normalsPatchFacesOriginal = np.asarray([pF.faceNormal for pF in patchFacesOriginal])
            if doRotate:
                vec = np.mean(np.asarray(
                    [fnm.faceNormal for fnm in [mModel.faces[j] for j in neighboursByFace(mModel, i, 4)[0]]]),
                    axis=0)
                # vec = np.mean(np.asarray([fnm.area * fnm.faceNormal for fnm in patchFacesOriginal]), axis=0)
                # vec = mModel.faces[i].faceNormal
                vec = vec / np.linalg.norm(vec)
                target = np.array([0.0, 1.0, 0.0])
                axis, theta = computeRotation(vec, target)
                normalsPatchFacesOriginal = rotatePatch(normalsPatchFacesOriginal, axis, theta)
            normalsPatchFacesOriginalR = normalsPatchFacesOriginal.reshape((patchSide, patchSide, 3))
            if reshapeFunction == "hilbert":
                for hci in range(np.shape(I2HC)[0]):
                    hicoord = HC2I[I2HC[hci, 0], I2HC[hci, 1]]
                    hx = I2HC[hci, 0]
                    hy = I2HC[hci, 1]
                    normalsPatchFacesOriginalR[hx, hy, :] = normalsPatchFacesOriginal[:, hicoord]
                    # normalsPatchFacesOriginalR[hc[0], hc[1], :] = normalsPatchFacesOriginal[:, hCoords.index(hc)]
            pIn = (normalsPatchFacesOriginalR + 1.0 * np.ones(np.shape(normalsPatchFacesOriginalR))) / 2.0
            pall.append(pIn)

        if mode == "PC":
            patchVerticesOriginal = [mModel.vertices[i] for i in neighboursByVertex(mModel, i, numOfElements)[0]]
            # patchFacesOriginal = [mModel.faces[i] for i in mModel.faces[i].neighbouringFaceIndices64[:numOfElements]]
            normalsPatchVerticesOriginal = np.asarray([pV.normal for pV in patchVerticesOriginal])
            for k in range(0, 2):
                normalsPatchVerticesOriginal[k, :] = np.array([0.0, 1.0, 0.0])
            if doRotate:
                vec = np.mean(np.asarray(
                    [vnm.normal for vnm in [mModel.vertices[j] for j in neighboursByVertex(mModel, i, 4)[0]]]),
                    axis=0)
                # vec = np.mean(np.asarray([fnm.area * fnm.faceNormal for fnm in patchFacesOriginal]), axis=0)
                # vec = mModel.faces[i].faceNormal
                vec = vec / np.linalg.norm(vec)
                target = np.array([0.0, 1.0, 0.0])
                axis, theta = computeRotation(vec, target)
                normalsPatchVerticesOriginal = rotatePatch(normalsPatchVerticesOriginal, axis, theta)

            normalsPatchVerticesOriginalR = normalsPatchVerticesOriginal.reshape((patchSide, patchSide, 3))
            if reshapeFunction == "hilbert":
                for hci in range(np.shape(I2HC)[0]):
                    hicoord = HC2I[I2HC[hci, 0], I2HC[hci, 1]]
                    hx = I2HC[hci, 0]
                    hy = I2HC[hci, 1]
                    normalsPatchVerticesOriginalR[hx, hy, :] = normalsPatchVerticesOriginal[:, hicoord]
                    # normalsPatchFacesOriginalR[hc[0], hc[1], :] = normalsPatchFacesOriginal[:, hCoords.index(hc)]
            pIn = (normalsPatchVerticesOriginalR + 1.0 * np.ones(np.shape(normalsPatchVerticesOriginalR))) / 2.0
            pall.append(pIn)
    x_batch = np.asarray(pall)
    result = saliency_model.predict(x_batch)

    if type=='discrete':
        result=np.expand_dims(result.argmax(axis=1),axis=1)/num_classes
    prediction[istart:iend] = result




step = (1 / saliencyDivisions)
showConfMat = True
showTags = True
if showConfMat:
    _true = np.clip((np.floor((gtdata/ step))).astype(int), a_min=0,
                    a_max=(saliencyDivisions - 1)).astype(
        int).tolist()
    _pred = np.clip((np.floor((prediction / step))).astype(int), a_min=0,
                    a_max=(saliencyDivisions - 1)).astype(int).tolist()




    classes = np.asarray(range(0, saliencyDivisions)).astype(int).tolist()
    normalize = True
    cm = confusion_matrix(_true, _pred,labels=list(range(saliencyDivisions)))
    print(cm)
    # Only use the labels that appear in the data
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')
    print(cm)
    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
    ax.figure.colorbar(im, ax=ax)
    if showTags:
        ax.set(xticks=np.arange(cm.shape[1]),
               yticks=np.arange(cm.shape[0]),
               # ... and label them with the respective list entries
               xticklabels=classes, yticklabels=classes,
               title='',
               ylabel='True label',
               xlabel='Predicted label')
        # Rotate the tick labels and set their alignment.
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
                 rotation_mode="anchor")
        fmt = '.2f' if normalize else 'd'
        thresh = cm.max() / 2.
        for i in range(cm.shape[0]):
            for j in range(cm.shape[1]):
                ax.text(j, i, format(cm[i, j], fmt),
                        ha="center", va="center", fontsize=18,
                        color="white" if cm[i, j] > thresh else "black")
    #fig.tight_layout()
    #fig.savefig('./results/cms/' + modelName + '_' + reshapeFunction + '_cnn_' + 'p_' + str(numOfElements),dpi=fig.dpi)
    plt.show()


prediction = np.asarray(prediction)

if mode == "MESH":
    resultPerVertex = np.zeros((len(mModel.vertices)))
    for mVertexIndex, mVertex in enumerate(mModel.vertices):
        umbrella = [prediction[mFaceIndex] for mFaceIndex in mVertex.neighbouringFaceIndices]
        umbrella = np.asarray(umbrella)
        resultPerVertex[mVertexIndex] = np.max(umbrella)
if mode == "PC":
    resultPerVertex = prediction




for i, v in enumerate(mModel.vertices):
    # h=-((resultPerVertex[i] * 240*0.25))
    h = 0
    # s=1.0
    s = 0
    # v=1.0
    v = (resultPerVertex[i])
    r, b, g = hsv2rgb(h, s, v)
    mModel.vertices[i] = mModel.vertices[i]._replace(color=np.asarray([r / 255, g / 255, b / 255]))

ffname = rootdir + modelsDir + modelName + "_" + reshapeFunction + "_pred" + "_cnn_" + "p_" + str(numOfElements)+"_"+keyword
exportObj(mModel, ffname + ".obj", color=True)
convert.execute(ffname + ".obj", ffname + ".ply")
# os.remove(ffname + ".obj")




if mode == "MESH":
    gtDataPerVertex = np.zeros((len(mModel.vertices)))
    for mVertexIndex, mVertex in enumerate(mModel.vertices):
        umbrella = [gtdata[mFaceIndex] for mFaceIndex in mVertex.neighbouringFaceIndices]
        umbrella = np.asarray(umbrella)
        gtDataPerVertex[mVertexIndex] = np.max(umbrella)
if mode == "PC":
    gtDataPerVertex = gtdata





doWriteModel=True

import open3d as o3d

pcdgt = o3d.geometry.PointCloud()
pcdgt.points = o3d.utility.Vector3dVector(np.asarray([v.position for v in mModel.vertices]))
pcdgt.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=2.0, max_nn=50))



###################################################################
voxel_volume = o3d.geometry.VoxelGrid.create_from_point_cloud(pcdgt, voxel_size=0.02)
# o3d.visualization.draw_geometries([voxel_volume])
VX=voxel_volume.get_voxels()
# point_cloud_np = np.asarray(
#                 [voxel_volume.origin + pt.grid_index * voxel_volume.voxel_size for pt in voxel_volume.get_voxels()])
# Build correspondence map
CorrespondenceMap=[]
for vertexIndex, vert in enumerate(mModel.vertices):
    #print(vertexIndex)
    voxelIndex=voxel_volume.get_voxel(vert.position)
    CorrespondenceMap.append(voxelIndex)
CorrespondenceMap=np.asarray(CorrespondenceMap)
CorrespondenceMapSet = {(tuple(row)) for row in CorrespondenceMap}
CorrespondenceMapList=list(CorrespondenceMapSet)
CorrespondenceMapDict={}
for c in CorrespondenceMapList:
    #print(c)
    CorrespondenceMapDict[c]=[]
for vertexIndex, vert in enumerate(mModel.vertices):
    #print(vertexIndex)
    voxelIndex=voxel_volume.get_voxel(vert.position)
    CorrespondenceMapDict[tuple(voxelIndex)].append(vertexIndex)
# point_cloud_np = np.asarray(
#                 [voxel_volume.origin + pt.grid_index * voxel_volume.voxel_size for pt in voxel_volume.get_voxels()])
# values=np.empty((np.shape(point_cloud_np)[0],))
vgdats=[]
vpdats=[]
vpoints=[]
for pt in voxel_volume.get_voxels():
    voxPoint=voxel_volume.origin + pt.grid_index * voxel_volume.voxel_size
    relevantVertices=CorrespondenceMapDict[tuple(pt.grid_index)]
    gtPointValue=np.mean(gtDataPerVertex[relevantVertices])
    predPointValue=np.mean(resultPerVertex[relevantVertices])
    vgdats.append(gtPointValue)
    vpdats.append(predPointValue)
    vpoints.append(voxPoint)


gtdataVoxelized=np.asarray(vgdats)
predictionUpdatedVoxelized=np.asarray(vpdats)

# gtdataVoxelized[gtdataVoxelized>0.125]=0.9
predictionUpdatedVoxelized[predictionUpdatedVoxelized>0.125]=9.0

voxelizedModel=NumpyArrayToPointCloudStructure(np.asarray(vpoints))

if doWriteModel:
    for i, vert in enumerate(voxelizedModel.vertices):
        # h=-((resultPerVertex[i] * 240*0.25))
        h = 0
        # s=1.0
        s = 0
        # v=1.0
        v = predictionUpdatedVoxelized[i]
        r, b, g = hsv2rgb(h, s, v)
        # mModel.vertices[i] = mModel.vertices[i]._replace(color=np.asarray([r / 255, g / 255, b / 255]))
        # r, g, b = J(predictionUpdated[i])[:3]
        voxelizedModel.vertices[i] = voxelizedModel.vertices[i]._replace(color=np.asarray([r / 255, g / 255, b / 255]))
        #mModel.vertices[i] = mModel.vertices[i]._replace(position=meshdefvertices[i])
    fvname=ffname+"_voxelized"
    exportObj(voxelizedModel, fvname+".obj", color=True)


saliencyDivisions=2
step = (1 / saliencyDivisions)

if showConfMat:
    _true = np.clip((np.floor((gtdataVoxelized/ step))).astype(int), a_min=0,
                    a_max=(saliencyDivisions - 1)).astype(
        int).tolist()
    _pred = np.clip((np.floor((predictionUpdatedVoxelized / step))).astype(int), a_min=0,
                    a_max=(saliencyDivisions - 1)).astype(int).tolist()

    classes = np.asarray(range(0, saliencyDivisions)).astype(int).tolist()
    normalize = True
    cm = confusion_matrix(_true, _pred)
    # Only use the labels that appear in the data
    print(cm)
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')
    print(cm)
    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
    ax.figure.colorbar(im, ax=ax)
    if showTags:
        ax.set(xticks=np.arange(cm.shape[1]),
               yticks=np.arange(cm.shape[0]),
               # ... and label them with the respective list entries
               xticklabels=classes, yticklabels=classes,
               title='',
               ylabel='True label',
               xlabel='Predicted label')
        # Rotate the tick labels and set their alignment.
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
                 rotation_mode="anchor")
        fmt = '.2f' if normalize else 'd'
        thresh = cm.max() / 2.
        for i in range(cm.shape[0]):
            for j in range(cm.shape[1]):
                ax.text(j, i, format(cm[i, j], fmt),
                        ha="center", va="center", fontsize=18,
                        color="white" if cm[i, j] > thresh else "black")
    #fig.tight_layout()
    #fig.savefig('./results/cms/' + modelName + '_' + reshapeFunction + '_cnn_' + 'p_' + str(numOfElements),dpi=fig.dpi)
    plt.show()

