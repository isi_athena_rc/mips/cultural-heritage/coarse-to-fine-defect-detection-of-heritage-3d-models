import copy
import numpy as np
from scipy import linalg as LA
import open3d as o3d
from common.commonReadOBJPointCloud import *
print("Testing IO for textured meshes ...")

rootDir='/home/stavros/Workspace/Mesh-Saliency-Extraction-Compression-Simplification/data/defects/pillar14/'
fname=rootDir+'pillar14v2.obj'
mesh = o3d.io.read_triangle_mesh(fname)

# mModel = loadObj(fname)
# updateGeometryAttibutes(mModel, useGuided=False, numOfFacesForGuided=32, computeDeltas=False,
#                         computeAdjacency=False, computeVertexNormals=False)

pc=np.asarray(mesh.vertices)
#centering the data
oo=np.mean(pc, axis = 0)
pc -= oo

for v_idx,v in enumerate(mesh.vertices):
    mesh.vertices[v_idx] = pc[v_idx]
o3d.io.write_triangle_mesh(rootDir+'pillar14v2_original_centered.obj',mesh)

pcd1 = o3d.geometry.PointCloud()
pcd1.points = o3d.utility.Vector3dVector(np.asarray(mesh.vertices))




cov = np.cov(pc, rowvar = False)
evals , evecs = LA.eigh(cov)
idx = np.argsort(evals)[::-1]
evecs = evecs[:,idx]
evals = evals[idx]


nn1=evecs[:,1]/np.linalg.norm(evecs[:,1])
nn2=evecs[:,2]/np.linalg.norm(evecs[:,2])
# nn=(nn1+nn2)/2
# nn=nn/np.linalg.norm(nn)


nn=-nn1


for v_idx,v in enumerate(mesh.vertices):
    dott=np.dot(nn,v-oo)
    r=v-dott*nn
    sym=2*r-v
    mesh.vertices[v_idx]=sym
    # print("OK")
o3d.io.write_triangle_mesh(rootDir+'pillar14v2_with_tranlation.obj',mesh)
pcd10 = o3d.geometry.PointCloud()
pcd10.points = o3d.utility.Vector3dVector(np.asarray(mesh.vertices))


# transformation=np.eye(4)
# transformation[0,3]=1
# transformation[1,3]=1
# transformation[2,3]=1
# pcd10=copy.deepcopy(pcd1).transform(transformation)
# for v_idx,v in enumerate(mesh.vertices):
#     mesh.vertices[v_idx] = pcd10.points[v_idx]
# o3d.io.write_triangle_mesh(rootDir+'pillar14v2_with_tranlation.obj',mesh)






threshold = 20
trans_init=np.eye(4)
pcd1.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.5, max_nn=50))
pcd10.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.5, max_nn=50))

for i in range(10):
    regresult = o3d.pipelines.registration.registration_icp(pcd1, pcd10, threshold, trans_init,
                                                       o3d.pipelines.registration.TransformationEstimationPointToPlane())
    # pcd10.transform(np.linalg.inv(regresult.transformation))
    pcd1.transform(regresult.transformation)
    print(regresult.transformation)





for v_idx,v in enumerate(mesh.vertices):
    mesh.vertices[v_idx] = pcd1.points[v_idx]
o3d.io.write_triangle_mesh(rootDir+'pillar14v2_registered.obj',mesh)






if True:
    pcorigin=copy.deepcopy(pcd1)
    pctarget=copy.deepcopy(pcd10)
    regresult1 = o3d.pipelines.registration.registration_icp(pcorigin, pctarget, 0.051, trans_init,
                                                           o3d.pipelines.registration.TransformationEstimationPointToPlane())

    pcorigin=copy.deepcopy(pcd1)
    pctarget=copy.deepcopy(pcd10)
    regresult2 = o3d.pipelines.registration.registration_icp(pcorigin, pctarget, 0.05, trans_init,
                                                           o3d.pipelines.registration.TransformationEstimationPointToPlane())
    all=   [*range(len(mesh.vertices))]
    p1=np.asarray(regresult1.correspondence_set)[:,0].tolist()
    p1bar=list(set(all) - set(p1))
    p2=np.asarray(regresult2.correspondence_set)[:,0].tolist()
    p2bar=list(set(all) - set(p2))
    p3=list(set(p2bar) - set(p1bar))


    # p3=p2bar

    # def intersection(lst1, lst2):
    #     lst3 = [value for value in lst1 if value in lst2]
    #     return lst3
    # def filterset(lst1, lst2):
    #     return list(set(lst1) - set(lst2))
    # p3=filterset(p1, p2)

    print(len(p3))

    # print(regresult.correspondence_set)
    fill = np.zeros(len(mesh.vertices)).astype(bool)
    # A=np.asarray(regresult.correspondence_set)[:,1]
    A=np.asarray(p3)
    fill[A] = True
    # fill = np.invert(fill)
    pcorigin = pcorigin.select_by_index(p3)
    o3d.io.write_point_cloud(rootDir+'pctest.xyz', pcorigin)





if False:

    saliencyvaluefilepath='pillar14v2_saliency_16_models_meshreshaping_hilbertcontinuous_16_continuoushilbert_pred_cnn_p_256_result.csv'
    saliencyvalues=rootDir+saliencyvaluefilepath


    saliencyPerFace = np.genfromtxt(saliencyvalues, delimiter=',')
    saliencyPerVertex = np.zeros((len(mModel.vertices)))
    for mVertexIndex, mVertex in enumerate(mModel.vertices):
        umbrella = [saliencyPerFace[mFaceIndex] for mFaceIndex in mVertex.neighbouringFaceIndices]
        umbrella = np.asarray(umbrella)
        saliencyPerVertex[mVertexIndex] = np.max(umbrella)

    pcorigin=copy.deepcopy(pcd1)
    pctarget=copy.deepcopy(pcd10)
    regresult1 = o3d.pipelines.registration.registration_icp(pcorigin, pctarget, 20.0, trans_init,
                                                           o3d.pipelines.registration.TransformationEstimationPointToPlane())

    p2=np.asarray(regresult1.correspondence_set).tolist()
    p3=[]
    for idx,row in enumerate(p2):
        if np.abs(saliencyPerVertex[row[0]]-saliencyPerVertex[row[1]])>0.5:
            p3.append(idx)

    fill = np.zeros(len(mesh.vertices)).astype(bool)
    # A=np.asarray(regresult.correspondence_set)[:,1]
    A=np.asarray(p3)
    fill[A] = True
    # fill = np.invert(fill)
    pctarget = pctarget.select_by_index(np.where(fill)[0])
    o3d.io.write_point_cloud(rootDir+'pctest_saliency.xyz', pctarget)


# pcd1to10=copy.deepcopy(pcd1).transform(regresult.transformation)
# for v_idx,v in enumerate(mesh.vertices):
#     mesh.vertices[v_idx] = pcd1to10.points[v_idx]
# o3d.io.write_triangle_mesh(rootDir+'pillar14v2_registered.obj',mesh)
#
# print(regresult.transformation)


# transformation=np.eye(4)
# transformation[:,3]=regresult.transformation[:,3]
# pcd3=copy.deepcopy(pcd2).transform(transformation)
# for v_idx,v in enumerate(mesh.vertices):
#     mesh.vertices[v_idx] = pcd3.points[v_idx]
# o3d.io.write_triangle_mesh(rootDir+'pillar14v2_trans.obj',mesh)
#

# print(regresult.transformation)
# print()
# print(transformation)
#
# fill = np.zeros(len(mesh.vertices)).astype(bool)
# A=np.asarray(regresult.correspondence_set)[:,0]
# fill[A] = True
# fill = np.invert(fill)
# pcd1 = pcd1.select_by_index(np.where(fill)[0])
#
# o3d.io.write_point_cloud(rootDir+'pctest.xyz', pcd1)
#
# fill = np.zeros(len(mesh.vertices)).astype(bool)
# A=np.asarray(regresult.correspondence_set)[:,1]
# fill[A] = True
# fill = np.invert(fill)
# pcd2 = pcd2.select_by_index(np.where(fill)[0])
#
# threshold = 0.2
# trans_init=np.eye(4)
# pcd1.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.5, max_nn=50))
# pcd2.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=0.5, max_nn=50))
# regresult = o3d.pipelines.registration.registration_icp(pcd1, pcd2, threshold, trans_init,
#                                                    o3d.pipelines.registration.TransformationEstimationPointToPlane())
