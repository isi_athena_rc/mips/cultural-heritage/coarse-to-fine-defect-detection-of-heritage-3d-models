import copy
import numpy as np
from scipy import linalg as LA
import open3d as o3d
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plt
from common.commonReadOBJPointCloud import *
print("Testing IO for textured meshes ...")

J=mycmap = plt.cm.get_cmap('jet')
r,g,b=J(0.1)[:3]

rootDir='/home/stavros/Workspace/Mesh-Saliency-Extraction-Compression-Simplification/data/PillarsWithDefects/'
mmodelname='pillar4RemeshedDefectsV2'
fnamegt=rootDir+mmodelname+'.obj'
fnamegtclustering=rootDir+mmodelname+'_clustering_outcome_Gerasimos.obj'
annotClustering=rootDir+mmodelname+'_annotation_clustering_Gerasimos.csv'

matchedoutputfnm=rootDir+mmodelname+'_annotation_clustering.csv'
annotGT=rootDir+mmodelname+'_annotation_of_points.csv'

keyword="_testing"

doWriteCSV=False

doWriteModel=not doWriteCSV
showConfMat = not doWriteCSV
showTags = not doWriteCSV

threshold=0.36
doapplyThreshold=True

prediction = np.genfromtxt(annotClustering, delimiter=',')
gtdata = np.genfromtxt(annotGT, delimiter=',')

if doapplyThreshold:
    prediction[prediction<=0.36]=0.0
    prediction[prediction>0.36]=0.8
    gtdata[gtdata<=0.05]=0.0
    gtdata[gtdata>0.05]=0.8


print(mmodelname)
mModel = loadObj(fnamegt)
mModelClustering = loadObj(fnamegtclustering)
pcdgt = o3d.geometry.PointCloud()
pcdgt.points = o3d.utility.Vector3dVector(np.asarray([v.position for v in mModel.vertices]))
pcdgt.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=2.0, max_nn=50))

pcddef = o3d.geometry.PointCloud()
pcddef.points = o3d.utility.Vector3dVector(np.asarray([v.position for v in mModelClustering.vertices]))
pcddef.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=2.0, max_nn=50))

threshold = 200
trans_init=np.eye(4)
regresult = o3d.pipelines.registration.registration_icp(pcdgt,pcddef,  threshold, trans_init,
                                                        o3d.pipelines.registration.TransformationEstimationPointToPlane())
p1=np.asarray(regresult.correspondence_set).tolist()
predictionUpdated = np.empty((np.shape(prediction)))






###################################################################


for correspondenceSet in p1:
    predictionUpdated[correspondenceSet[0]]=prediction[correspondenceSet[1]]

if doWriteCSV:
    np.savetxt(matchedoutputfnm,predictionUpdated,delimiter='\n',fmt='%1.3f')










if doWriteModel:
    for i, vert in enumerate(mModel.vertices):
        # h=-((resultPerVertex[i] * 240*0.25))
        h = 0
        # s=1.0
        s = 0
        # v=1.0
        v = predictionUpdated[i]
        r, b, g = hsv2rgb(h, s, v)
        # mModel.vertices[i] = mModel.vertices[i]._replace(color=np.asarray([r / 255, g / 255, b / 255]))
        r, g, b = J(predictionUpdated[i])[:3]
        r=r*255
        b=b*255
        g=g*255
        mModel.vertices[i] = mModel.vertices[i]._replace(color=np.asarray([r/ 255, g/ 255 , b/ 255 ]))
        #mModel.vertices[i] = mModel.vertices[i]._replace(position=meshdefvertices[i])
    exportObj(mModel, rootDir + mmodelname+"_clustering"+keyword+".obj", color=True)


saliencyDivisions=2
step = (1 / saliencyDivisions)

if showConfMat:
    _true = np.clip((np.floor((gtdata/ step))).astype(int), a_min=0,
                    a_max=(saliencyDivisions - 1)).astype(
        int).tolist()
    _pred = np.clip((np.floor((predictionUpdated / step))).astype(int), a_min=0,
                    a_max=(saliencyDivisions - 1)).astype(int).tolist()

    classes = np.asarray(range(0, saliencyDivisions)).astype(int).tolist()
    normalize = True
    cm = confusion_matrix(_true, _pred)
    # Only use the labels that appear in the data
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')
    print(cm)
    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
    ax.figure.colorbar(im, ax=ax)
    if showTags:
        ax.set(xticks=np.arange(cm.shape[1]),
               yticks=np.arange(cm.shape[0]),
               # ... and label them with the respective list entries
               xticklabels=classes, yticklabels=classes,
               title='',
               ylabel='True label',
               xlabel='Predicted label')
        # Rotate the tick labels and set their alignment.
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
                 rotation_mode="anchor")
        fmt = '.2f' if normalize else 'd'
        thresh = cm.max() / 2.
        for i in range(cm.shape[0]):
            for j in range(cm.shape[1]):
                ax.text(j, i, format(cm[i, j], fmt),
                        ha="center", va="center", fontsize=18,
                        color="white" if cm[i, j] > thresh else "black")
    #fig.tight_layout()
    #fig.savefig('./results/cms/' + modelName + '_' + reshapeFunction + '_cnn_' + 'p_' + str(numOfElements),dpi=fig.dpi)
    plt.show()





###################################################################
voxel_volume = o3d.geometry.VoxelGrid.create_from_point_cloud(pcdgt, voxel_size=0.02)
# o3d.visualization.draw_geometries([voxel_volume])
VX=voxel_volume.get_voxels()
# point_cloud_np = np.asarray(
#                 [voxel_volume.origin + pt.grid_index * voxel_volume.voxel_size for pt in voxel_volume.get_voxels()])
# Build correspondence map
CorrespondenceMap=[]
for vertexIndex, vert in enumerate(mModel.vertices):
    #print(vertexIndex)
    voxelIndex=voxel_volume.get_voxel(vert.position)
    CorrespondenceMap.append(voxelIndex)
CorrespondenceMap=np.asarray(CorrespondenceMap)
CorrespondenceMapSet = {(tuple(row)) for row in CorrespondenceMap}
CorrespondenceMapList=list(CorrespondenceMapSet)
CorrespondenceMapDict={}
for c in CorrespondenceMapList:
    #print(c)
    CorrespondenceMapDict[c]=[]
for vertexIndex, vert in enumerate(mModel.vertices):
    #print(vertexIndex)
    voxelIndex=voxel_volume.get_voxel(vert.position)
    CorrespondenceMapDict[tuple(voxelIndex)].append(vertexIndex)
# point_cloud_np = np.asarray(
#                 [voxel_volume.origin + pt.grid_index * voxel_volume.voxel_size for pt in voxel_volume.get_voxels()])
# values=np.empty((np.shape(point_cloud_np)[0],))
vgdats=[]
vpdats=[]
vpoints=[]
for pt in voxel_volume.get_voxels():
    voxPoint=voxel_volume.origin + pt.grid_index * voxel_volume.voxel_size
    relevantVertices=CorrespondenceMapDict[tuple(pt.grid_index)]
    #DD = CorrespondenceMapList.index(tuple(pt.grid_index))
    gtPointValue=np.mean(gtdata[relevantVertices])
    predPointValue=np.mean(predictionUpdated[relevantVertices])
    vgdats.append(gtPointValue)
    vpdats.append(predPointValue)
    vpoints.append(voxPoint)


gtdataVoxelized=np.asarray(vgdats)
predictionUpdatedVoxelized=np.asarray(vpdats)

# gtdataVoxelized[gtdataVoxelized>0]=1.0
# predictionUpdatedVoxelized[predictionUpdatedVoxelized>0]=1.0

voxelizedModel=NumpyArrayToPointCloudStructure(np.asarray(vpoints))

if doWriteModel:
    for i, vert in enumerate(voxelizedModel.vertices):
        # h=-((resultPerVertex[i] * 240*0.25))
        h = 0
        # s=1.0
        s = 0
        # v=1.0
        v = gtdataVoxelized[i]
        r, b, g = hsv2rgb(h, s, v)
        # mModel.vertices[i] = mModel.vertices[i]._replace(color=np.asarray([r / 255, g / 255, b / 255]))
        # r, g, b = J(predictionUpdated[i])[:3]
        voxelizedModel.vertices[i] = voxelizedModel.vertices[i]._replace(color=np.asarray([r / 255, g/ 255, b/ 255]))
        # mModel.vertices[i] = mModel.vertices[i]._replace(position=meshdefvertices[i])
    exportObj(voxelizedModel, rootDir + mmodelname + "_groundtruth_voxelized" + keyword + ".obj", color=True)

    for i, vert in enumerate(voxelizedModel.vertices):
        # h=-((resultPerVertex[i] * 240*0.25))
        h = 0
        # s=1.0
        s = 0
        # v=1.0
        v = predictionUpdatedVoxelized[i]
        r, b, g = hsv2rgb(h, s, v)
        # mModel.vertices[i] = mModel.vertices[i]._replace(color=np.asarray([r / 255, g / 255, b / 255]))
        # r, g, b = J(predictionUpdated[i])[:3]
        voxelizedModel.vertices[i] = voxelizedModel.vertices[i]._replace(color=np.asarray([r/ 255, g/ 255 , b/ 255 ]))
        #mModel.vertices[i] = mModel.vertices[i]._replace(position=meshdefvertices[i])
    exportObj(voxelizedModel, rootDir + mmodelname+"_clustering_voxelized"+keyword+".obj", color=True)


saliencyDivisions=2
step = (1 / saliencyDivisions)

if showConfMat:
    _true = np.clip((np.floor((gtdataVoxelized/ step))).astype(int), a_min=0,
                    a_max=(saliencyDivisions - 1)).astype(
        int).tolist()
    _pred = np.clip((np.floor((predictionUpdatedVoxelized / step))).astype(int), a_min=0,
                    a_max=(saliencyDivisions - 1)).astype(int).tolist()

    classes = np.asarray(range(0, saliencyDivisions)).astype(int).tolist()
    normalize = True
    cm = confusion_matrix(_true, _pred)
    # Only use the labels that appear in the data

    print(cm)
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')
    print(cm)
    fig, ax = plt.subplots()
    im = ax.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
    ax.figure.colorbar(im, ax=ax)
    if showTags:
        ax.set(xticks=np.arange(cm.shape[1]),
               yticks=np.arange(cm.shape[0]),
               # ... and label them with the respective list entries
               xticklabels=classes, yticklabels=classes,
               title='',
               ylabel='True label',
               xlabel='Predicted label')
        # Rotate the tick labels and set their alignment.
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
                 rotation_mode="anchor")
        fmt = '.2f' if normalize else 'd'
        thresh = cm.max() / 2.
        for i in range(cm.shape[0]):
            for j in range(cm.shape[1]):
                ax.text(j, i, format(cm[i, j], fmt),
                        ha="center", va="center", fontsize=18,
                        color="white" if cm[i, j] > thresh else "black")
    #fig.tight_layout()
    #fig.savefig('./results/cms/' + modelName + '_' + reshapeFunction + '_cnn_' + 'p_' + str(numOfElements),dpi=fig.dpi)
    plt.show()

