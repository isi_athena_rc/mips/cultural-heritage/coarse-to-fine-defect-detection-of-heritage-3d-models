from common.definitions import *
from config.configTrainDefect02 import *
from common.nets import CNNmodelKeras,CNNDeepmodelKeras
from keras.callbacks import ModelCheckpoint
saliency_model=CNNDeepmodelKeras(img_size,num_channels,num_classes,type)
train_data=[]
train_labels=[]
# trainSet=['pillar14']
trainSet=['pillar9RemeshedDefectsV1','pillar9RemeshedDefectsV2']
groundTruthKeyword='_annotation_of_centroids'
clusteringKeyword='_annotation_clustering'


saliencyValues=[]
print(keyTrain)
for modelName in trainSet:
    # ======Model information=====================================================================
    mModelSrc = rootdir + modelsDir + modelName + '.obj'
    print(modelName)
    if mode == "MESH":
        mModel = loadObj(mModelSrc)
        updateGeometryAttibutes(mModel, useGuided=useGuided, numOfFacesForGuided=patchSizeGuided, computeDeltas=False,
                                computeAdjacency=False, computeVertexNormals=False)
    if mode == "PC":
        mModel = loadObjPC(mModelSrc, nn=pointcloudnn, simplify=presimplification)
        V, inds = computePointCloudNormals(mModel, pointcloudnn)
        exportPLYPC(mModel, modelsDir + modelName + '_pcnorm_conf.ply')


    gtdata=np.genfromtxt(rootdir + modelsDir + modelName+groundTruthKeyword+'.csv', delimiter=',')

    clusteringData=np.genfromtxt(rootdir + modelsDir + modelName+clusteringKeyword+'.csv', delimiter=',')


    clusteringDataPerFace = np.zeros((len(mModel.faces)))
    for mFaceIndex, mFace in enumerate(mModel.faces):
        triangle = [clusteringData[mVertexIndex] for mVertexIndex in mFace.verticesIndices]
        triangle = np.asarray(triangle)
        clusteringDataPerFace[mFaceIndex] = np.max(triangle)



    # #saliencyValue=saliencyValue/np.max(saliencyValue)
    print('Saliency ground truth data')
    if type == 'continuous':
        # saliencyValues=gtdata.tolist()
        for v in gtdata.tolist():
            saliencyValues.append(v)
    if type == 'discrete':
        for s in gtdata.tolist():
            v = int((num_classes - 1) * s)
            saliencyValues.append(v)

    if True:

        if mode == "MESH":
            iLen = len(mModel.faces)
            patches = [neighboursByFace(mModel, i, numOfElements)[0] for i in range(0, len(mModel.faces))]
            # Rotation and train data formulation===============================================================================
            for i, p in enumerate(patches):
                print(i)
                patchFacesOriginal = [mModel.faces[i] for i in p]
                positionsPatchFacesOriginal=np.asarray([pF.centroid for pF in patchFacesOriginal])
                normalsPatchFacesOriginal = np.asarray([pF.faceNormal for pF in patchFacesOriginal])
                # vec = np.mean(np.asarray(
                #         [fnm.faceNormal for fnm in [mModel.faces[j] for j in neighboursByFace(mModel, i, 4)[0]]]
                #     ), axis=0)
                vec = np.mean(np.asarray([fnm.area * fnm.faceNormal for fnm in patchFacesOriginal]), axis=0)
                normalsPatchFacesOriginal = np.asarray([pF.faceNormal if pF.area>0 else vec for pF in patchFacesOriginal])

                clusteringPatch = np.asarray([clusteringDataPerFace[i] for i in p])
                clusteringPatch = np.expand_dims(clusteringPatch, axis=0)
                clusteringPatchR = clusteringPatch.reshape((patchSide, patchSide, 1))



                # vec = mModel.faces[i].faceNormal
                vec = vec / np.linalg.norm(vec)
                axis, theta = computeRotation(vec, target)
                normalsPatchFacesOriginal = rotatePatch(normalsPatchFacesOriginal, axis, theta)
                normalsPatchFacesOriginalR = normalsPatchFacesOriginal.reshape((patchSide, patchSide, 3))
                if reshapeFunction == "hilbert":
                    for hci in range(np.shape(I2HC)[0]):
                        normalsPatchFacesOriginalR[I2HC[hci, 0], I2HC[hci, 1], :] = normalsPatchFacesOriginal[:,
                                                                                    HC2I[I2HC[hci, 0], I2HC[hci, 1]]]
                        clusteringPatchR[I2HC[hci, 0], I2HC[hci, 1], 0] = clusteringPatch[0, HC2I[I2HC[hci, 0], I2HC[hci, 1]]]

                pPatch=(normalsPatchFacesOriginalR + 1.0 * np.ones(np.shape(normalsPatchFacesOriginalR))) / 2.0
                pPatch=np.concatenate((pPatch,clusteringPatchR),axis=2)
                # print(np.shape(pPatch))
                # exit(4)
                train_data.append(pPatch)

        if mode == "PC":
            iLen = len(mModel.vertices)
            patches = [neighboursByVertex(mModel, i, numOfElements)[0] for i in range(0, len(mModel.vertices))]
            # patches = np.random.choice(patches, numOfElements, replace=False)
            for i, p in enumerate(patches):
                print(i)
                patchVerticesOriginal = [mModel.vertices[i] for i in p]
                normalsPatchVerticesOriginal = np.asarray([pF.normal for pF in patchVerticesOriginal])
                vec = np.mean(np.asarray([fnm.normal for fnm in patchVerticesOriginal]), axis=0)
                vec = vec / np.linalg.norm(vec)
                axis, theta = computeRotation(vec, target)
                normalsPatchVerticesOriginal = rotatePatch(normalsPatchVerticesOriginal, axis, theta)
                normalsPatchVerticesOriginalR = normalsPatchVerticesOriginal.reshape((patchSide, patchSide, 3))
                if reshapeFunction == "hilbert":
                    for hci in range(np.shape(I2HC)[0]):
                        normalsPatchVerticesOriginalR[I2HC[hci, 0], I2HC[hci, 1], :] = normalsPatchVerticesOriginal[:,
                                                                                    HC2I[I2HC[hci, 0], I2HC[hci, 1]]]
                train_data.append((normalsPatchVerticesOriginalR + 1.0 * np.ones(np.shape(normalsPatchVerticesOriginalR))) / 2.0)


        with open('test_fused.pkl','wb') as f:
            pickle.dump(train_data, f)

    else:
        with open('test_fused.pkl', 'rb') as file:
            train_data = pickle.load(file)

checkpoint = ModelCheckpoint(sessionsDir+keyTrain +'_best_model.h5', monitor='loss', verbose=1,
    save_best_only=True, mode='auto', period=5)

# Dataset and labels summarization ========================================================================
if type == 'continuous':
    seppoint = int(dtsetsplit * np.shape(train_data)[0])
    train_data = np.asarray(train_data)
    train_labels = np.asarray([np.asarray(saliencyValues)]).transpose()
    X=train_data[:seppoint]
    X_test=train_data[seppoint:]
    Y=np.asarray(saliencyValues[:seppoint])
    Y_test = np.asarray(saliencyValues[seppoint:])
    data_train= X
    data_test = X_test
    label_train=Y
    label_test=Y_test
    saliency_model.compile(loss=tf.keras.losses.MeanSquaredError(), optimizer='adam', metrics=[tf.keras.metrics.RootMeanSquaredError()])

if type == 'discrete':
    seppoint = int(dtsetsplit * np.shape(train_data)[0])
    train_data = np.asarray(train_data)
    X=train_data[:seppoint]
    X_test=train_data[seppoint:]
    Y=saliencyValues[:seppoint]
    Y_test = saliencyValues[seppoint:]
    data_train= X
    data_test = X_test
    label_train=to_categorical(Y,num_classes=num_classes)
    label_test=to_categorical(Y_test,num_classes=num_classes)
    saliency_model.compile(loss='categorical_crossentropy', optimizer='adam',metrics=['accuracy'])

saliency_model.summary()

# saliency_model.load_weights( sessionsDir+keyTrain +'.h5')
# saliency_model.load_weights(sessionsDir+keyTrain +'.h5')

saliency_model_train = saliency_model.fit(x=data_train, y=label_train, batch_size=batch_size, epochs=numEpochs, verbose=1,callbacks=[checkpoint])

saliency_model.save( sessionsDir+keyTrain +'.h5')

