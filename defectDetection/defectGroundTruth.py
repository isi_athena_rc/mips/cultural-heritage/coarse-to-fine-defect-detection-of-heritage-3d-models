import copy
import numpy as np
from scipy import linalg as LA
import open3d as o3d
from common.commonReadOBJPointCloud import *
print("Testing IO for textured meshes ...")

rootDir='/home/stavros/Workspace/Mesh-Saliency-Extraction-Compression-Simplification/data/PillarsWithDefects/'
modelNamegt='pillar4Remeshed'
modelNamedef='pillar4RemeshedDefectsV2'
doWrite=False

fnamegt=rootDir+modelNamegt+'.obj'

meshgt = o3d.io.read_triangle_mesh(fnamegt)

o3d.io.write_triangle_mesh(fnamegt,meshgt)

meshgtvertices=np.asarray(meshgt.vertices)

pcdgt = o3d.geometry.PointCloud()

pcdgt.points = o3d.utility.Vector3dVector(meshgtvertices)

pcdgt.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=2.0, max_nn=50))

fnamedef=rootDir+modelNamedef+'.obj'

meshdef = o3d.io.read_triangle_mesh(fnamedef)

o3d.io.write_triangle_mesh(fnamedef,meshdef)

meshdefvertices=np.asarray(meshdef.vertices)

pcddef = o3d.geometry.PointCloud()

pcddef.points = o3d.utility.Vector3dVector(meshdefvertices)

pcddef.estimate_normals(search_param=o3d.geometry.KDTreeSearchParamHybrid(radius=2.0, max_nn=50))

# threshold = 200
# trans_init=np.eye(4)
# regresult = o3d.pipelines.registration.registration_icp(pcddef,pcdgt,  threshold, trans_init,
#                                                         o3d.pipelines.registration.TransformationEstimationPointToPlane())
# p1=np.asarray(regresult.correspondence_set).tolist()
# dists=np.empty((len(p1),))
# for index,ptuple in enumerate(p1):
#     dists[index]= np.linalg.norm(meshdefvertices[p1[0]]-meshgtvertices[p1[1]])
# dists=dists/np.max(dists)

dist_pc1_pc2 = pcddef.compute_point_cloud_distance(pcdgt)
dists=np.asarray(dist_pc1_pc2)
dists=dists/np.max(dists)


dists[dists>0.05]=0.8
dists[dists<=0.05]=0.0
#dists[(dists>0.05) & (dists<0.125)]=0.125




mModel = loadObj(fnamedef)
valuePerVertexIn = dists
valuePerFaceOut = np.zeros((len(mModel.faces)))
for mFaceIndex, mFace in enumerate(mModel.faces):
    triangle = [valuePerVertexIn[mVertexIndex] for mVertexIndex in mFace.verticesIndices]
    triangle = np.asarray(triangle)
    valuePerFaceOut[mFaceIndex] = np.max(triangle)

if doWrite:
    np.savetxt(rootDir + modelNamedef +'_annotation_of_centroids'+'.csv',valuePerFaceOut.reshape(1, valuePerFaceOut.shape[0]), delimiter='\n',fmt="%f")
    np.savetxt(rootDir + modelNamedef +'_annotation_of_points'+'.csv',valuePerVertexIn.reshape(1, valuePerVertexIn.shape[0]), delimiter='\n',fmt="%f")

for i, vert in enumerate(mModel.vertices):
    # h=-((resultPerVertex[i] * 240*0.25))
    h = 0
    # s=1.0
    s = 0
    # v=1.0
    v = dists[i]
    r, b, g = hsv2rgb(h, s, v)
    mModel.vertices[i] = mModel.vertices[i]._replace(color=np.asarray([r/255, g/255, b/255 ]))
    #mModel.vertices[i] = mModel.vertices[i]._replace(position=meshdefvertices[i])

exportObj(mModel, rootDir + modelNamedef+"_groundtruth"+".obj", color=True)

print("OK")





# for index,pointstuple in enumerate(p1):
#     diff=meshdefvertices[pointstuple[0]] - meshgtvertices[pointstuple[1]]
#     dists[index]=np.linalg.norm(diff)






