# Copyright 2019 Florent Mahoudeau. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

import os.path
import numpy as np
import tensorflow as tf


def fcn8():
    # Block 1

    with tf.compat.v1.name_scope('conv1_1') as scope:
        kernel = tf.Variable(weights['conv1_1_W'], name='weights', dtype=tf.float32, expected_shape=[3, 3, 3, 64])
        conv = tf.nn.conv2d(input=images, filters=kernel, strides=[1, 1, 1, 1], padding='SAME')
        bias = tf.Variable(weights['conv1_1_b'], name='biases', dtype=tf.float32, expected_shape=[64])
        out = tf.nn.bias_add(conv, bias)
        self.conv1_1 = tf.nn.relu(out, name=scope)

    with tf.compat.v1.name_scope('conv1_2') as scope:
        kernel = tf.Variable(weights['conv1_2_W'], name='weights', dtype=tf.float32, expected_shape=[3, 3, 64, 64])
        conv = tf.nn.conv2d(input=self.conv1_1, filters=kernel, strides=[1, 1, 1, 1], padding='SAME')
        bias = tf.Variable(weights['conv1_2_b'], name='biases', dtype=tf.float32, expected_shape=[64])
        out = tf.nn.bias_add(conv, bias)
        self.conv1_2 = tf.nn.relu(out, name=scope)

    self.pool1 = tf.nn.max_pool2d(input=self.conv1_2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1],
                                  padding='SAME', name='pool1')

    # Block 2
    with tf.compat.v1.name_scope('conv2_1') as scope:
        kernel = tf.Variable(weights['conv2_1_W'], name='weights', dtype=tf.float32, expected_shape=[3, 3, 64, 128])
        conv = tf.nn.conv2d(input=self.pool1, filters=kernel, strides=[1, 1, 1, 1], padding='SAME')
        bias = tf.Variable(weights['conv2_1_b'], name='biases', dtype=tf.float32, expected_shape=[128])
        out = tf.nn.bias_add(conv, bias)
        self.conv2_1 = tf.nn.relu(out, name=scope)

    with tf.compat.v1.name_scope('conv2_2') as scope:
        kernel = tf.Variable(weights['conv2_2_W'], name='weights', dtype=tf.float32,
                             expected_shape=[3, 3, 128, 128])
        conv = tf.nn.conv2d(input=self.conv2_1, filters=kernel, strides=[1, 1, 1, 1], padding='SAME')
        bias = tf.Variable(weights['conv2_2_b'], name='biases', dtype=tf.float32, expected_shape=[128])
        out = tf.nn.bias_add(conv, bias)
        self.conv2_2 = tf.nn.relu(out, name=scope)

    self.pool2 = tf.nn.max_pool2d(input=self.conv2_2, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1],
                                  padding='SAME', name='pool2')

    # Block 3
    with tf.compat.v1.name_scope('conv3_1') as scope:
        kernel = tf.Variable(weights['conv3_1_W'], name='weights', dtype=tf.float32,
                             expected_shape=[3, 3, 128, 256])
        conv = tf.nn.conv2d(input=self.pool2, filters=kernel, strides=[1, 1, 1, 1], padding='SAME')
        bias = tf.Variable(weights['conv3_1_b'], name='biases', dtype=tf.float32, expected_shape=[256])
        out = tf.nn.bias_add(conv, bias)
        self.conv3_1 = tf.nn.relu(out, name=scope)

    with tf.compat.v1.name_scope('conv3_2') as scope:
        kernel = tf.Variable(weights['conv3_2_W'], name='weights', dtype=tf.float32,
                             expected_shape=[3, 3, 256, 256])
        conv = tf.nn.conv2d(input=self.conv3_1, filters=kernel, strides=[1, 1, 1, 1], padding='SAME')
        bias = tf.Variable(weights['conv3_2_b'], name='biases', dtype=tf.float32, expected_shape=[256])
        out = tf.nn.bias_add(conv, bias)
        self.conv3_2 = tf.nn.relu(out, name=scope)

    with tf.compat.v1.name_scope('conv3_3') as scope:
        kernel = tf.Variable(weights['conv3_3_W'], name='weights', dtype=tf.float32,
                             expected_shape=[3, 3, 256, 256])
        conv = tf.nn.conv2d(input=self.conv3_2, filters=kernel, strides=[1, 1, 1, 1], padding='SAME')
        bias = tf.Variable(weights['conv3_3_b'], name='biases', dtype=tf.float32, expected_shape=[256])
        out = tf.nn.bias_add(conv, bias)
        self.conv3_3 = tf.nn.relu(out, name=scope)

    self.pool3 = tf.nn.max_pool2d(input=self.conv3_3, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1],
                                  padding='SAME', name='pool3')
    self.layer3_out = tf.identity(self.pool3, name='layer3_out')

    # Block 4
    with tf.compat.v1.name_scope('conv4_1') as scope:
        kernel = tf.Variable(weights['conv4_1_W'], name='weights', dtype=tf.float32,
                             expected_shape=[3, 3, 256, 512])
        conv = tf.nn.conv2d(input=self.pool3, filters=kernel, strides=[1, 1, 1, 1], padding='SAME')
        bias = tf.Variable(weights['conv4_1_b'], name='biases', dtype=tf.float32, expected_shape=[512])
        out = tf.nn.bias_add(conv, bias)
        self.conv4_1 = tf.nn.relu(out, name=scope)

    with tf.compat.v1.name_scope('conv4_2') as scope:
        kernel = tf.Variable(weights['conv4_2_W'], name='weights', dtype=tf.float32,
                             expected_shape=[3, 3, 512, 512])
        conv = tf.nn.conv2d(input=self.conv4_1, filters=kernel, strides=[1, 1, 1, 1], padding='SAME')
        bias = tf.Variable(weights['conv4_2_b'], name='biases', dtype=tf.float32, expected_shape=[512])
        out = tf.nn.bias_add(conv, bias)
        self.conv4_2 = tf.nn.relu(out, name=scope)

    with tf.compat.v1.name_scope('conv4_3') as scope:
        kernel = tf.Variable(weights['conv4_3_W'], name='weights', dtype=tf.float32,
                             expected_shape=[3, 3, 512, 512])
        conv = tf.nn.conv2d(input=self.conv4_2, filters=kernel, strides=[1, 1, 1, 1], padding='SAME')
        bias = tf.Variable(weights['conv4_3_b'], name='biases', dtype=tf.float32, expected_shape=[512])
        out = tf.nn.bias_add(conv, bias)
        self.conv4_3 = tf.nn.relu(out, name=scope)

    self.pool4 = tf.nn.max_pool2d(input=self.conv4_3, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1],
                                  padding='SAME', name='pool4')
    self.layer4_out = tf.identity(self.pool4, name='layer4_out')

    # Block 5
    with tf.compat.v1.name_scope('conv5_1') as scope:
        kernel = tf.Variable(weights['conv5_1_W'], name='weights', dtype=tf.float32,
                             expected_shape=[3, 3, 512, 512])
        conv = tf.nn.conv2d(input=self.pool4, filters=kernel, strides=[1, 1, 1, 1], padding='SAME')
        bias = tf.Variable(weights['conv5_1_b'], name='biases', dtype=tf.float32, expected_shape=[512])
        out = tf.nn.bias_add(conv, bias)
        self.conv5_1 = tf.nn.relu(out, name=scope)

    with tf.compat.v1.name_scope('conv5_2') as scope:
        kernel = tf.Variable(weights['conv5_2_W'], name='weights', dtype=tf.float32,
                             expected_shape=[3, 3, 512, 512])
        conv = tf.nn.conv2d(input=self.conv5_1, filters=kernel, strides=[1, 1, 1, 1], padding='SAME')
        bias = tf.Variable(weights['conv5_2_b'], name='biases', dtype=tf.float32, expected_shape=[512])
        out = tf.nn.bias_add(conv, bias)
        self.conv5_2 = tf.nn.relu(out, name=scope)

    with tf.compat.v1.name_scope('conv5_3') as scope:
        kernel = tf.Variable(weights['conv5_3_W'], name='weights', dtype=tf.float32,
                             expected_shape=[3, 3, 512, 512])
        conv = tf.nn.conv2d(input=self.conv5_2, filters=kernel, strides=[1, 1, 1, 1], padding='SAME')
        bias = tf.Variable(weights['conv5_3_b'], name='biases', dtype=tf.float32, expected_shape=[512])
        out = tf.nn.bias_add(conv, bias)
        self.conv5_3 = tf.nn.relu(out, name=scope)

    self.pool5 = tf.nn.max_pool2d(input=self.conv5_3, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1],
                                  padding='SAME', name='pool5')

    # Block 6 is a convolutionized version of fc6 with kernel shape (25088, 4096)
    with tf.compat.v1.name_scope('conv6') as scope:
        kernel = tf.Variable(weights['fc6_W'].reshape(7, 7, 512, 4096), name='weights',
                             dtype=tf.float32, expected_shape=[7, 7, 512, 4096])
        conv = tf.nn.conv2d(input=self.pool5, filters=kernel, strides=[1, 1, 1, 1], padding='SAME')
        bias = tf.Variable(weights['fc6_b'], name='biases', dtype=tf.float32, expected_shape=[4096])
        out = tf.nn.bias_add(conv, bias)
        self.conv6 = tf.nn.relu(out, name=scope)

    self.drop1 = tf.nn.dropout(self.conv6, name='dropout1', rate=self.dropout_rate)

    # Block 7 is a convolutionized version of fc7 with kernel shape (4096, 4096)
    with tf.compat.v1.name_scope('conv7') as scope:
        kernel = tf.Variable(weights['fc7_W'].reshape(1, 1, 4096, 4096), name='weights',
                             dtype=tf.float32, expected_shape=[1, 1, 4096, 4096])
        conv = tf.nn.conv2d(input=self.drop1, filters=kernel, strides=[1, 1, 1, 1], padding='SAME')
        bias = tf.Variable(weights['fc7_b'], name='biases', dtype=tf.float32, expected_shape=[4096])
        out = tf.nn.bias_add(conv, bias)
        self.conv7 = tf.nn.relu(out, name=scope)

    self.drop2 = tf.nn.dropout(self.conv7, name='dropout2', rate=self.dropout_rate)
    self.layer7_out = tf.identity(self.drop2, name='layer7_out')