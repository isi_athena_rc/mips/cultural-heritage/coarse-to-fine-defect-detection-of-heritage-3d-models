import sys
import glob
import os
sys.path.append('D:/_GroundWork/frugally-deep')

from keras_export.convert_model import convert as frugallycov


modeldir='./models/'
kerasmodelpaths= [f for f in glob.glob(modeldir+'*.h5')]
for kpath in kerasmodelpaths:
 basename = os.path.basename(kpath)
 fnm,ext=os.path.splitext(basename)
 frugallycov(kpath,modeldir+fnm+'.json',no_tests = False)
 print('OK')