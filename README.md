# Coarse-to-fine defect detection of heritage 3D models using a CNN learning approach


## Introduction

Protection of endangered heritage objects or buildings is highly challenging and usually requires on-site visual inspection of experts to detect if any further intervention of a professional is required. Nevertheless, this type of inspection is time-consuming and not suitable for quantitative analysis. 
In this direction, 3D model analysis techniques have been proposed to detect 3D defects as an alternative method to on-site inspections. In this work, we propose a coarse-to-fine defect detection approach. It includes an initial step for the data preparation, where all patches, representing a neighbouring area 
of each point of the 3D structure, are aligned (translated and rotated) so as to have the same orientation and the same ground point. Next, a feature vector is estimated for each point which is then used for an initial/coarse defect detection. Although, most of the defects can be identified during this step, to further improve our method we use a CNN-based learning approach, which receives the output of the previous step, to fine-tune the detecting accuracy. Extensive evaluation studies using a variety of heritage 3D objects verify that the proposed approach provides promising results (74% accuracy for successfully detecting defects and 80% for  detecting non-defects, respectively) to the automatic defect detection of 3D model surfaces.

## How to use


### Dependencies

To be described

### Training

```
python defectDetection/saliencyTrainDefect02.py
```

### Prediction
```
python defectDetection/saliencyPredictDefect02.py
```

## Citation

```
G. Arvanitis, S. Nousias, A. S. Lalos, and K. Moustakas, 
“Coarse-to-fine defects detection of heritage 3D objects using 
a CNN learning approach,” Int. Conf. Ind. Cyber-Physical Syst., 2022.
```

```
@inproceedings{arvanitis2022coarse,
author = {Arvanitis, Gerasimos and Nousias, Stavros and Lalos, Aris S and Moustakas, Konstantinos},
booktitle = {International Conference on Industrial Cyber-Physical Systems},
title = {{Coarse-to-fine defects detection of heritage 3D objects using a CNN learning approach}},
year = {2022}
}

```


